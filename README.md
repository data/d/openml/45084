# OpenML dataset: 11_Tumors

https://www.openml.org/d/45084

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**11 Tumors dataset**

**Authors**: A. Su, J. Welsh, L. Sapinoso, S. Kern, P. Dimitrov, H. Lapp, P. Schultz, S. Powell, C. Moskaluk, H. Frierson Jr., et al

**Please cite**: ([URL](https://www.scopus.com/record/display.uri?eid=2-s2.0-0035887459&origin=inward)): A. Su, J. Welsh, L. Sapinoso, S. Kern, P. Dimitrov, H. Lapp, P. Schultz, S. Powell, C. Moskaluk, H. Frierson Jr., et al, Molecular classification of human carcinomas by use of gene expression signatures, Cancer Res. 61 (20) (2001) 7388-7393

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45084) of an [OpenML dataset](https://www.openml.org/d/45084). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45084/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45084/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45084/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

